package com.milos.app.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.milos.app.bean.Group;
import com.milos.app.bean.Table;

public interface GroupMapper {
	Group getGroup(@Param("name")String name, @Param("leagueId")Integer leagueId);
	void insertGroup(Group group);
	Table getTable(Integer groupId);
	List<Group> getGroups(@Param("groupFilter")List<String> groupFilter);
}
