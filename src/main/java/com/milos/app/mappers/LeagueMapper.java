package com.milos.app.mappers;

import com.milos.app.bean.League;

public interface LeagueMapper {
	League getLeague(String name);
	void insertLeague(League league);
}
