package com.milos.app.mappers;

import com.milos.app.bean.Team;

public interface TeamMapper {
	Team getTeam(String name);
	void insertTeam(Team team);
	void updateTeam(Team team);
}
