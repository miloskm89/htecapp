package com.milos.app.mappers;

import java.util.List;

import com.milos.app.bean.Match;
import com.milos.app.bean.MatchInput;
import com.milos.app.bean.ResultFilter;

public interface MatchMapper {
	void insertMatch(Match match);
	List<MatchInput> getMatches(ResultFilter filter);
	Match getMatch(MatchInput match);
	void editMatch(Match match);
}
