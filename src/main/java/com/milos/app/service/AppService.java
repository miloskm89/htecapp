package com.milos.app.service;

import java.util.List;

import com.milos.app.bean.MatchInput;
import com.milos.app.bean.ResultFilter;
import com.milos.app.bean.Table;

public interface AppService {
	public List<Table> processMatches(List<MatchInput> matches);
	public List<Table> getStandings(List<String> groups);
	public List<MatchInput> getMatches(ResultFilter filter);
	public List<Table> editMatches(List<MatchInput> matches);
}
