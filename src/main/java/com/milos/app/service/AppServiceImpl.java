package com.milos.app.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milos.app.bean.Group;
import com.milos.app.bean.League;
import com.milos.app.bean.Match;
import com.milos.app.bean.MatchInput;
import com.milos.app.bean.ResultFilter;
import com.milos.app.bean.Score;
import com.milos.app.bean.Table;
import com.milos.app.bean.TableTeam;
import com.milos.app.bean.Team;
import com.milos.app.mappers.GroupMapper;
import com.milos.app.mappers.LeagueMapper;
import com.milos.app.mappers.MatchMapper;
import com.milos.app.mappers.TeamMapper;

@Service
public class AppServiceImpl implements AppService {
	
	@Autowired
	private LeagueMapper leagueMapper;
	@Autowired
	private GroupMapper groupMapper;
	@Autowired
	private TeamMapper teamMapper;
	@Autowired
	private MatchMapper matchMapper;

	@Override
	public List<Table> processMatches(List<MatchInput> matches) {
		for (MatchInput matchInput : matches) {
			League league = leagueMapper.getLeague(matchInput.getLeagueTitle());
 			if (league == null) {
 				league = new League();
 				league.setName(matchInput.getLeagueTitle());
 				leagueMapper.insertLeague(league);
			}
 			
 			Group group = groupMapper.getGroup(matchInput.getGroup(), league.getId());
 			if (group == null) {
 				group = new Group();
 				group.setName(matchInput.getGroup());
 				group.setLeague(league);
 				groupMapper.insertGroup(group);
 			}
 			
 			boolean newHomeTeam = false;
 			Team homeTeam = teamMapper.getTeam(matchInput.getHomeTeam());
 			if (homeTeam == null) {
 				newHomeTeam = true;
 				homeTeam = new Team();
 				homeTeam.setName(matchInput.getHomeTeam());
 				homeTeam.setGroup(group);
 			}
 			homeTeam.setPlayedGames(homeTeam.getPlayedGames() + 1);
 			homeTeam.setGoals(homeTeam.getGoals() + matchInput.getScore().getHomeGoals());
 			homeTeam.setGoalsAgainst(homeTeam.getGoalsAgainst() + matchInput.getScore().getAwayGoals());
 			
 			boolean newAwayTeam = false;
 			Team awayTeam = teamMapper.getTeam(matchInput.getAwayTeam());
 			if (awayTeam == null) {
 				newAwayTeam = true;
 				awayTeam = new Team();
 				awayTeam.setName(matchInput.getAwayTeam());
 				awayTeam.setGroup(group);
 			}
 			awayTeam.setPlayedGames(awayTeam.getPlayedGames() + 1);
 			awayTeam.setGoals(awayTeam.getGoals() + matchInput.getScore().getAwayGoals());
 			awayTeam.setGoalsAgainst(awayTeam.getGoalsAgainst() + matchInput.getScore().getHomeGoals());
 			
 			switch (matchInput.getScore().getOutcome()) {
				case HOME_WIN:
					homeTeam.setPoints(homeTeam.getPoints() + 3);
					homeTeam.setWin(homeTeam.getWin() + 1);
					
					awayTeam.setLose(awayTeam.getLose() + 1);
					break;
				case AWAY_WIN:
					awayTeam.setPoints(awayTeam.getPoints() + 3);
					awayTeam.setWin(awayTeam.getWin() + 1);
					
					homeTeam.setLose(homeTeam.getLose() + 1);
					break;
				default:
					homeTeam.setPoints(homeTeam.getPoints() + 1);
					homeTeam.setDraw(homeTeam.getDraw() + 1);
					
					awayTeam.setPoints(awayTeam.getPoints() + 1);
					awayTeam.setDraw(awayTeam.getDraw() + 1);
					break;
			}
			
			if (newHomeTeam) {
				teamMapper.insertTeam(homeTeam);
			} else {
				teamMapper.updateTeam(homeTeam);
			}
			
			if (newAwayTeam) {
				teamMapper.insertTeam(awayTeam);
			} else {
				teamMapper.updateTeam(awayTeam);
			}
 			
			Match match = new Match();
			match.setMatchday(matchInput.getMatchday());
			match.setGroup(group);
			match.setHomeTeam(homeTeam);
			match.setAwayTeam(awayTeam);
			match.setKickoffAt(matchInput.getKickoffAt());
			match.setHomeGoals(matchInput.getScore().getHomeGoals());
			match.setAwayGoals(matchInput.getScore().getAwayGoals());
			matchMapper.insertMatch(match);
		}
		
		List<Group> groups = groupMapper.getGroups(null);
		List<Table> tablesToReturn = new ArrayList<Table>();
		Iterator<Group> iterator = groups.iterator();
		while(iterator.hasNext()) {
			Group group = iterator.next();
			Table table = groupMapper.getTable(group.getId());
			if (table != null && table.getStanding() != null) {
				int i = 1;
				for(TableTeam team : table.getStanding()) {
					team.setRank(i);
					i++;
				}
			}
			tablesToReturn.add(table);
		}
		
		return tablesToReturn;
	}

	@Override
	public List<Table> getStandings(List<String> groupFilter) {
		List<Table> tablesToReturn = new ArrayList<Table>();
		List<Group> groups = groupMapper.getGroups(groupFilter);
		Iterator<Group> iterator = groups.iterator();
		while(iterator.hasNext()) {
			Group group = iterator.next();
			Table table = groupMapper.getTable(group.getId());
			if (table != null && table.getStanding() != null) {
				int i = 1;
				for(TableTeam team : table.getStanding()) {
					team.setRank(i);
					i++;
				}
			}
			tablesToReturn.add(table);
		}
		 
		return tablesToReturn;
	}

	@Override
	public List<MatchInput> getMatches(ResultFilter filter) {
		if (filter != null) {
			if (filter.getDateStart() != null && !filter.getDateStart().isEmpty()) {
				try {
					String pattern = "yyyy-MM-dd'T'HH:mm:ss";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			
					Date realDate = simpleDateFormat.parse(filter.getDateStart());
					filter.setRealDateStart(realDate);
				} catch (Exception e) {
				}
			}
			
			if (filter.getDateEnd() != null && !filter.getDateEnd().isEmpty()) {
				try {
					String pattern = "yyyy-MM-dd'T'HH:mm:ss";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			
					Date realDate = simpleDateFormat.parse(filter.getDateEnd());
					filter.setRealDateEnd(realDate);
				} catch (Exception e) {
				}
			}
		}
		List<MatchInput> list = matchMapper.getMatches(filter);
		return list;
	}

	@Override
	public List<Table> editMatches(List<MatchInput> matches) {
		List<Table> tablesToReturn = new ArrayList<Table>();
		if (matches != null) {
			for (MatchInput match : matches) {
				Match matchFromDB = matchMapper.getMatch(match);
				
				if (!match.getScore().getOutcome().equals(matchFromDB.getOutcome())) {
					Team homeTeam = teamMapper.getTeam(match.getHomeTeam());
					Team awayTeam = teamMapper.getTeam(match.getAwayTeam());
					
					homeTeam.setGoals(homeTeam.getGoals() - matchFromDB.getHomeGoals() + match.getScore().getHomeGoals());
					homeTeam.setGoalsAgainst(homeTeam.getGoalsAgainst() - matchFromDB.getAwayGoals() + match.getScore().getAwayGoals());
					
					awayTeam.setGoals(awayTeam.getGoals() - matchFromDB.getAwayGoals() + match.getScore().getAwayGoals());
					awayTeam.setGoalsAgainst(awayTeam.getGoalsAgainst() - matchFromDB.getHomeGoals() + match.getScore().getHomeGoals());
					
					if (matchFromDB.getOutcome().equals(Score.Outcome.HOME_WIN)) {
						homeTeam.setWin(homeTeam.getWin() - 1);
						awayTeam.setLose(awayTeam.getLose() - 1);
						if (match.getScore().getOutcome().equals(Score.Outcome.AWAY_WIN)) {
							homeTeam.setLose(homeTeam.getLose() + 1);
							homeTeam.setPoints(homeTeam.getPoints() - 3);
							
							awayTeam.setWin(awayTeam.getWin() + 1);
							awayTeam.setPoints(awayTeam.getPoints() + 3);
						} else {
							homeTeam.setDraw(homeTeam.getDraw() + 1);
							homeTeam.setPoints(homeTeam.getPoints() - 2);
							
							awayTeam.setDraw(awayTeam.getDraw() + 1);
							awayTeam.setPoints(awayTeam.getPoints() + 1);
						}
					} else if (matchFromDB.getOutcome().equals(Score.Outcome.AWAY_WIN)) {
						homeTeam.setLose(homeTeam.getLose() - 1);
						awayTeam.setWin(awayTeam.getWin() - 1);
						if (match.getScore().getOutcome().equals(Score.Outcome.HOME_WIN)) {
							homeTeam.setWin(homeTeam.getWin() + 1);
							homeTeam.setPoints(homeTeam.getPoints() + 3);
							
							awayTeam.setLose(awayTeam.getLose() + 1);
							awayTeam.setPoints(awayTeam.getPoints() - 3);
						} else {
							homeTeam.setDraw(homeTeam.getDraw() + 1);
							homeTeam.setPoints(homeTeam.getPoints() + 1);
							
							awayTeam.setDraw(awayTeam.getDraw() + 1);
							awayTeam.setPoints(awayTeam.getPoints() - 2);
						}
					} else {
						homeTeam.setDraw(homeTeam.getDraw() - 1);
						awayTeam.setDraw(awayTeam.getDraw() - 1);
						if (match.getScore().getOutcome().equals(Score.Outcome.HOME_WIN)) {
							homeTeam.setWin(homeTeam.getWin() + 1);
							homeTeam.setPoints(homeTeam.getPoints() + 2);
							
							awayTeam.setLose(awayTeam.getLose() + 1);
							awayTeam.setPoints(awayTeam.getPoints() - 1);
						} else {
							homeTeam.setLose(homeTeam.getLose() + 1);
							homeTeam.setPoints(homeTeam.getPoints() - 1);
							
							awayTeam.setWin(awayTeam.getWin() + 1);
							awayTeam.setPoints(awayTeam.getPoints() + 2);
						}
					}
					
					teamMapper.updateTeam(homeTeam);
					teamMapper.updateTeam(awayTeam);
				}
				
				matchFromDB.setHomeGoals(match.getScore().getHomeGoals());
				matchFromDB.setAwayGoals(match.getScore().getAwayGoals());
				
				matchMapper.editMatch(matchFromDB);
			}
			
			List<Group> groups = groupMapper.getGroups(null);
			Iterator<Group> iterator = groups.iterator();
			while(iterator.hasNext()) {
				Group group = iterator.next();
				Table table = groupMapper.getTable(group.getId());
				if (table != null && table.getStanding() != null) {
					int i = 1;
					for(TableTeam team : table.getStanding()) {
						team.setRank(i);
						i++;
					}
				}
				tablesToReturn.add(table);
			}
		}
		
		return tablesToReturn;
	}

}
