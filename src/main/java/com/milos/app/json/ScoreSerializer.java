package com.milos.app.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.milos.app.bean.Score;

public class ScoreSerializer extends StdSerializer<Score> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScoreSerializer() { 
        this(null); 
    } 
 
    public ScoreSerializer(Class<Score> vc) { 
        super(vc); 
    }

	@Override
	public void serialize(Score score, JsonGenerator jGen, SerializerProvider provider) throws IOException {
		String toWrite = score.getHomeGoals() + ":" + score.getAwayGoals();
		
		jGen.writeString(toWrite);
	}

}
