package com.milos.app.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.milos.app.bean.Score;

public class ScoreDeserializer extends StdDeserializer<Score> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScoreDeserializer() { 
        this(null); 
    } 
 
    public ScoreDeserializer(Class<?> vc) { 
        super(vc); 
    }

	@Override
	public Score deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		
		String fullScore = node.asText();
		
		if (fullScore != null && !fullScore.isEmpty() && fullScore.contains(":")) {
			String[] scores = fullScore.split(":");
			
			if (scores.length == 2) {
				return new Score(Integer.parseInt(scores[0]), Integer.parseInt(scores[1]));
			}
		}
        
        return new Score();
	}

}
