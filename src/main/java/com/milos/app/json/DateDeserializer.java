package com.milos.app.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DateDeserializer extends StdDeserializer<Date> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateDeserializer() { 
        this(null); 
    } 
 
    public DateDeserializer(Class<?> vc) { 
        super(vc); 
    }

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		
		String date = node.asText();
		
		try {
			String pattern = "yyyy-MM-dd'T'HH:mm:ss";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

			Date realDate = simpleDateFormat.parse(date);
			return realDate;
		} catch (Exception e) {
			return null;
		}
	}

}
