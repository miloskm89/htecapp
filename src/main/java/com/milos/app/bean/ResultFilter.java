package com.milos.app.bean;

import java.util.Date;
import java.util.List;

public class ResultFilter {
	private String dateStart;
	private Date realDateStart;
	private String dateEnd;
	private Date realDateEnd;
	private List<String> groups;
	private List<String> teams;
	
	public String getDateStart() {
		return dateStart;
	}
	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}
	public Date getRealDateStart() {
		return realDateStart;
	}
	public void setRealDateStart(Date realDateStart) {
		this.realDateStart = realDateStart;
	}
	public String getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}
	public Date getRealDateEnd() {
		return realDateEnd;
	}
	public void setRealDateEnd(Date realDateEnd) {
		this.realDateEnd = realDateEnd;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	public List<String> getTeams() {
		return teams;
	}
	public void setTeams(List<String> teams) {
		this.teams = teams;
	}
	
}
