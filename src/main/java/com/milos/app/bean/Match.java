package com.milos.app.bean;

import java.util.Date;

public class Match {
	private int id;
	private Group group;
	private int matchday;
	private Team homeTeam;
	private Team awayTeam;
	private Date kickoffAt;
	private int homeGoals;
	private int awayGoals;
	private Score.Outcome outcome;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMatchday() {
		return matchday;
	}
	public void setMatchday(int matchday) {
		this.matchday = matchday;
	}
	public Date getKickoffAt() {
		return kickoffAt;
	}
	public void setKickoffAt(Date kickoffAt) {
		this.kickoffAt = kickoffAt;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Team getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}
	public Team getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}
	public int getHomeGoals() {
		return homeGoals;
	}
	public void setHomeGoals(int homeGoals) {
		this.homeGoals = homeGoals;
	}
	public int getAwayGoals() {
		return awayGoals;
	}
	public void setAwayGoals(int awayGoals) {
		this.awayGoals = awayGoals;
	}
	public Score.Outcome getOutcome() {
		return outcome;
	}
	public void setOutcome(Score.Outcome outcome) {
		this.outcome = outcome;
	}
}
