package com.milos.app.bean;

public class Score {
	
	public enum Outcome {
		HOME_WIN,
		AWAY_WIN,
		DRAW
	}
	
	public Score(int homeGoals, int awayGoals) {
		this.homeGoals = homeGoals;
		this.awayGoals = awayGoals;
		setOutcome();
	}
	
	public Score() {		
	}

	private int homeGoals;
	private int awayGoals;
	private Outcome outcome;
	
	public int getHomeGoals() {
		return homeGoals;
	}

	public void setHomeGoals(int homeGoals) {
		this.homeGoals = homeGoals;
		setOutcome();
	}

	public int getAwayGoals() {
		return awayGoals;
	}

	public void setAwayGoals(int awayGoals) {
		this.awayGoals = awayGoals;
		setOutcome();
	}

	public Outcome getOutcome() {
		return outcome;
	}

	private void setOutcome() {
		if (homeGoals > awayGoals) {
			this.outcome = Outcome.HOME_WIN;
		} else if (homeGoals < awayGoals) {
			this.outcome = Outcome.AWAY_WIN;
		} else {
			this.outcome = Outcome.DRAW;
		}
	}
	
}
