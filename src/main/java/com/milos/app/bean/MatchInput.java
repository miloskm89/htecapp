package com.milos.app.bean;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.milos.app.json.DateSerializer;
import com.milos.app.json.ScoreDeserializer;
import com.milos.app.json.ScoreSerializer;

public class MatchInput {
	private String leagueTitle;
	private int matchday;
	private String group;
	private String homeTeam;
	private String awayTeam;
	@JsonSerialize(using=DateSerializer.class)
	private Date kickoffAt;
	@JsonDeserialize(using=ScoreDeserializer.class)
	@JsonSerialize(using=ScoreSerializer.class)
	private Score score;
	
	public String getLeagueTitle() {
		return leagueTitle;
	}
	public void setLeagueTitle(String leagueTitle) {
		this.leagueTitle = leagueTitle;
	}
	public int getMatchday() {
		return matchday;
	}
	public void setMatchday(int matchday) {
		this.matchday = matchday;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}
	public String getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}
	public Date getKickoffAt() {
		return kickoffAt;
	}
	public void setKickoffAt(Date kickoffAt) {
		this.kickoffAt = kickoffAt;
	}
	public Score getScore() {
		return score;
	}
	public void setScore(Score score) {
		this.score = score;
	}
}
