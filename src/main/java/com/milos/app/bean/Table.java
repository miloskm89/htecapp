package com.milos.app.bean;

import java.util.ArrayList;

public class Table {
	private String leagueTitle;
	private int matchday;
	private String group;
	private ArrayList<TableTeam> standing;
	
	public String getLeagueTitle() {
		return leagueTitle;
	}
	public void setLeagueTitle(String leagueTitle) {
		this.leagueTitle = leagueTitle;
	}
	public int getMatchday() {
		return matchday;
	}
	public void setMatchday(int matchday) {
		this.matchday = matchday;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public ArrayList<TableTeam> getStanding() {
		return standing;
	}
	public void setStanding(ArrayList<TableTeam> standing) {
		this.standing = standing;
	}
}
