package com.milos.app.bean;

public class Group {
	private int id;
	private String name;
	
	private League league;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + id;
		result = 31 * result + name.hashCode();
		if (league != null) {
			result = 31 * result + league.getId();
			result = 31 * result + league.getName().hashCode();
		}
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
        if (!(obj instanceof Group)) {
            return false;
        }

        Group group = (Group) obj;
        

        return group.id == id &&
        		group.name.equals(name) && ((group.league == null && league == null) || (group.league.getId() == league.getId() && group.league.getName().equals(league.getName())));
	}
}
