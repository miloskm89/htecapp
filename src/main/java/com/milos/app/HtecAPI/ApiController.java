package com.milos.app.HtecAPI;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.milos.app.bean.MatchInput;
import com.milos.app.bean.ResultFilter;
import com.milos.app.bean.Table;
import com.milos.app.service.AppService;

@RestController
public class ApiController {
	
	@Autowired
	public AppService appService;
	
	@RequestMapping(path="results", method=RequestMethod.POST)
	public List<Table> inputResults(@RequestBody List<MatchInput> matches) {
		return appService.processMatches(matches);
	}
	
	@RequestMapping(path="standings", method=RequestMethod.GET)
	public List<Table> getStandings(@RequestParam(required = false, name = "groups") List<String> groups) {
		return appService.getStandings(groups);
	}

	@RequestMapping(path="results", method=RequestMethod.GET)
	public List<MatchInput> getResults(@ModelAttribute ResultFilter filter) {
		return appService.getMatches(filter);
	}
	
	@RequestMapping(path="edit-results", method=RequestMethod.POST)
	public List<Table> editResults(@RequestBody List<MatchInput> matches) {
		return appService.editMatches(matches);
	}
}
