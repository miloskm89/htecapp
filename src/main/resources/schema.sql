CREATE TABLE Leagues
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(250) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE Groups
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(250) NOT NULL,
    league_id int(11) NOT NULL,
    FOREIGN KEY (league_id) REFERENCES Leagues(id),
    PRIMARY KEY (id)
);

CREATE TABLE Teams
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(250) NOT NULL UNIQUE,
    points int(11) NOT NULL,
    played_games int(11) NOT NULL,
    goals int(11) NOT NULL,
    goals_against int(11) NOT NULL,
    win int(11) NOT NULL,
    lose int(11) NOT NULL,
    draw int(11) NOT NULL,
    group_id int(11) NOT NULL,
    FOREIGN KEY (group_id) REFERENCES Groups(id),
    PRIMARY KEY (id)
);

CREATE TABLE Matches
(
    id int(11) NOT NULL AUTO_INCREMENT,
    matchday int(11) NOT NULL,
    group_id int(11) NOT NULL,
    home_team_id int(11) NOT NULL,
    away_team_id int(11) NOT NULL,
    kickoff_at DATETIME NOT NULL,
    home_goals int(11) NOT NULL,
    away_goals int(11) NOT NULL,
    FOREIGN KEY (group_id) REFERENCES Groups(id),
    FOREIGN KEY (home_team_id) REFERENCES Teams(id),
    FOREIGN KEY (away_team_id) REFERENCES Teams(id),
    PRIMARY KEY (id)
);